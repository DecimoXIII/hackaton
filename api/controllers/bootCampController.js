'use strict';


var mongoose = require('mongoose'),
  Movimientos = mongoose.model('Hackaton');

exports.list_all_tasks = function(req, res) {
  Movimientos.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.riskLevel = function(req, res) {

  Movimientos.findOneAndUpdate({ deviceId:"5516214891"}, {$inc : {nriesgo : 2}}, function(err, task) {
    if (err)
         res.send(err);

  });
  Movimientos.findOneAndUpdate({ deviceId:"7721291683"}, {$inc : {nriesgo : 3}}, function(err, task) {
    if (err)
         res.send(err);

  });
  Movimientos.findOneAndUpdate({ deviceId:"5533186847"}, {$set : {nriesgo : 15}}, function(err, task) {
    if (err)
         res.send(err);
       res.json(task);
  });

};

exports.add_movimiento = function(req, res) {
  console.log(req.params.taskId);
  Movimientos.updateOne({deviceId: req.params.taskId}, { $push: { movimientos: req.body }}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.transferencia = function(req, res) {
  console.log(req.body.emisor);
  console.log(req.body.monto);
  var aux = req.body.monto * -1;
  Movimientos.findOneAndUpdate({ deviceId:req.body.emisor, 'cuentas.tipoCuenta': "Debito"}, {$inc : {'cuentas.$.montoActual' : aux}}, function(err, task) {
    if (err)
      res.send(err);
  });
  Movimientos.findOneAndUpdate({ deviceId:req.body.receptor, 'cuentas.tipoCuenta': "Debito"}, {$inc : {'cuentas.$.montoActual' : req.body.monto}}, function(err, task) {
    if (err)
         res.send(err);
       res.json(task);
  });
};

exports.set_recomendador = function(req, res) {
  Movimientos.findOneAndUpdate({deviceId: req.body.deviceId}, { $set: { recomendador: req.body.recomendador }}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.set_recomendado = function(req, res) {
  Movimientos.findOneAndUpdate({deviceId: req.params.deviceId}, { $push: { recomendados: req.body }}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.list_offers = function(req, res) {
  console.log(req.body.deviceId);
  Movimientos.find({deviceId: req.body.deviceId},'ofertas',function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.get_a_password = function(req, res) {
  console.log(req.body.deviceId);
  Movimientos.find({deviceId: req.body.deviceId},'pass',function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.get_pushId = function(req, res) {
  console.log(req.body.deviceId);
  Movimientos.find({deviceId: req.body.deviceId},'pushId',function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.get_movimientos = function(req, res) {
  console.log(req.body.deviceId);
  Movimientos.find({deviceId: req.body.deviceId},'movimientos',function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.get_cuentas = function(req, res) {
  console.log(req.body.deviceId);
  Movimientos.find({deviceId: req.body.deviceId},'cuentas',function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};



exports.create_user = function(req, res) {
  console.log(req.body);
  var new_task = new Movimientos(req.body);
  new_task.save(function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

// exports.create_a_task = function(req, res) {
//   var new_task = new Movimientos(req.body);
//   new_task.save(function(err, task) {
//     if (err)
//       res.send(err);
//     res.json(task);
//   });
// };


exports.read_a_task = function(req, res) {
  console.log(req.params.taskId);
  Movimientos.findById(req.params.taskId, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.update_a_task = function(req, res) {
  console.log(req.params.taskId);
  Movimientos.findOneAndUpdate({deviceId: req.params.taskId}, { $push: { movimientos: req.body }}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.delete_a_task = function(req, res) {
  Task.remove({
    _id: req.params.taskId
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'Task successfully deleted' });
  });
};
