'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var HackatonSchema = new Schema({
  nombre: String,
  apellidoP: String,
  apellidoM: String,
  deviceId: String,
  pushId: String,
  pass: String,
  nriesgo: {type: Number, default: 15},
  recomendador:String,
  recomendados:[{rdeviceId: String, afectado: {type:Boolean, default: false}}],
  ofertas: [{lda: String, mensaje: String, monto: Number}],
  cuentas: [{tipoCuenta: String, montoActual: Number, nTarjeta: String}],
  movimientos: [{fecha:{type: Date, default: Date.now}, monto: Number, cuenta: String, comercio: String, ubicacion: { latitud: String, longitud: String}}]
});



// {
// "nombre": "Osvaldo",
// "apellidoP": "Alcantara",
// "apellidoM": "Guerrero",
// "deviceId": "5533186847",
// "pushId": "dpXqJmdNv5s:APA91bGuy9a70Bix41o4ri0rShnpcZObQty8PXw44KuAeebu0jnvHF90m7IA-Xht1wlN-P7QGx6kGhsIzKh05XeHbNT5o3lJ05XyBp77-acXjuluwXJBygSTcQIUpJ8Yo4n4M8tsMj7H",
// "pass": "1234",
// "ncliente": "B14444068",
// "cuentas":[
// {
// "tipoCuenta": "Debito",
// "montoActual": 1000,
// "nTarjeta": "4624244242442442"
// },
// {
// "tipoCuenta": "Credito",
// "montoActual": 15000,
// "nTarjeta": "4142342242442442"
// }
// ],
// "movimientos":[
// {
// "monto": 1000,
// "cuenta": "Debito",
// "comercio": "SEARS",
// "ubicacion":{"latitud": "-890203", "longitud": "694938"}
// }
// ],
//   "ofertas":[{"lda": "0568", "mensaje": "Felicidades tienes un credito hipotecario por: ", "monto": 1000000}]
// }

// {
//   "nombre": "Osvaldo",
//   "apellidoP": "Alcantara",
//   "apellidoM": "Guerrero",
//   "deviceId": "5533186847",
//   "pushId": "dpXqJmdNv5s:APA91bGuy9a70Bix41o4ri0rShnpcZObQty8PXw44KuAeebu0jnvHF90m7IA-Xht1wlN-P7QGx6kGhsIzKh05XeHbNT5o3lJ05XyBp77-acXjuluwXJBygSTcQIUpJ8Yo4n4M8tsMj7H",
//   "pass": "1234",
//   "ncliente": "B14444068",
//   "cuentas": [{"tipoCuenta": "Debito", "montoActual": 1000, "nTarjeta": "4624244242442442"}],
//   "movimientos": [{"monto": 500, "comercio": "SEARS", "ubicacion": { "latitud": "-890203", "longitud": "694938"}}]
// }

// var TaskSchema = new Schema({
//   name: {
//
// cuentas: [{tipoCuenta: String, montoActual: Number, nTarjeta: String}],
// ofertas: [{lda: String, mensaje: String, monto: Number}],
// movimientos: [{fecha:{type: Date, default: Date.now}, monto: Number, comercio: String, ubicacion: { latitud: String, longitud: String}}]

//     type: String,
//     required: 'Kindly enter the name of the task'
//   },
//   Created_date: {
//     type: Date,
//     default: Date.now
//   },
//   status: {
//     type: [{
//       type: String,
//       enum: ['pending', 'ongoing', 'completed']
//     }],
//     default: ['pending']
//   }
// });


// {
// "monto": 1000,
// "comercio": "Sears",
// "ubicacion":{"latitud": "-34.397", "longitud": "150.644"}
// }

// {
// "nombre": "Pancho Villa",
// "apellidoP": "Pérez",
// "apellidoM": "Sánchez"
// }

module.exports = mongoose.model('Hackaton', HackatonSchema);
