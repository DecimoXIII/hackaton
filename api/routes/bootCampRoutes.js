'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/bootCampController');

  app.route('/user')///5d6d7a584504a9cb4fd63768
    .get(todoList.list_all_tasks)
    .post(todoList.create_user);

  app.route('/riskLevel')///5d6d7a584504a9cb4fd63768
    //.get(todoList.list_all_tasks)
    .get(todoList.riskLevel);

  // todoList Routes
  app.route('/grantingTicket')///5d6d7a584504a9cb4fd63768
    //.get(todoList.list_all_tasks)
    .post(todoList.get_a_password);

  app.route('/pushId')///5d6d7a584504a9cb4fd63768
    //.get(todoList.list_all_tasks)
    .post(todoList.get_pushId);

    app.route('/transferencia')///5d6d7a584504a9cb4fd63768
      //.get(todoList.list_all_tasks)
      .post(todoList.transferencia);

    app.route('/listOffers')///5d6d7a584504a9cb4fd63768
      //.get(todoList.list_all_tasks)
      .post(todoList.list_offers);

      app.route('/setRecomendador')///5d6d7a584504a9cb4fd63768
        //.get(todoList.list_all_tasks)
        .post(todoList.set_recomendador);

        app.route('/setRecomendado/:deviceId')///5d6d7a584504a9cb4fd63768
          //.get(todoList.list_all_tasks)
          .put(todoList.set_recomendado);

    app.route('/cuentas')///5d6d7a584504a9cb4fd63768
      //.get(todoList.list_all_tasks)
      .post(todoList.get_cuentas);

      app.route('/movimientos')///5d6d7a584504a9cb4fd63768
      .post(todoList.get_movimientos);

  app.route('/movimientos/:taskId')///5d6d7a584504a9cb4fd63768
  .put(todoList.update_a_task);
    //.get(todoList.list_all_tasks)
    //.put(todoList.add_movimiento)




  // app.route('/movimientos/:taskId')
  //   // .get(todoList.read_a_task)
  //    .post(todoList.add_movimiento);
    // .delete(todoList.delete_a_task);
};
